import React from 'react';
import {NavComponent} from "../../components/index.js";
import './contact.scss'

function ContactPage() {
    return (
        <>
            <NavComponent/>
            <div className={"contact_container"}>
                <div className="contact_header">
                    <h3>Contact</h3>
                </div>
                <div className="contact_content">
                    <div className="contact_info_line">
                        test
                    </div>
                    <form action="">

                    </form>
                </div>
                
            </div>

        </>
    );
}

export default ContactPage;