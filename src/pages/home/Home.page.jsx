import React from 'react';
import {NavComponent} from "../../components/index.js";

import Souris from '/mx-master-3s.png'
import facebook from '/facebook_icon.png';
import linkedin from '/icons8-linkedin.png';
import {gsap, Power4} from "gsap";

import './home.scss'
function HomePage() {
    let tl = new gsap.timeline();
    let ease = Power4.easeOut();
    return (
        <div className="container">
            <NavComponent timeline={tl} ease={ease}/>
            <div className={"hero"} >
                <div className="left_side">
                    <h1>SOLUTION INFROMATIQUE <br/><span>SUR MESURE</span></h1>
                    <p>Boostez votre Entreprise en 2 CLIQUES</p>
                    <div>
                        <button>(1) CONTACTEZ-NOUS</button>
                    </div>
                </div>

                <div className="right_side">
                    <img src={Souris} alt="image d'une souris d'ordinateur" width={308} height={452}/>
                </div>

            </div>
            <div className="socials">
                <span>Suivez-nous</span>
                <div></div>

                <a href=""><img src={linkedin} alt="" width={24} height={24}/></a>
                <a href=""><img src={facebook} alt="" width={24} height={24}/></a>
            </div>
        </div>
    );
}

export default HomePage;