import {useRef, useEffect} from "react";
import Logo from "/images/ctrlkey_logo1_dark.png"
import './nav.scss'

import {gsap, Power4} from "gsap"
function NavComponent({timeline, ease}) {
    let logo = useRef(null);
    let link1 = useRef(null);
    let link2 = useRef(null);
    let link3 = useRef(null);
    let link4 = useRef(null);

    /*useEffect(()=>{
        console.log(logo)
        timeline.from(logo, .5, {opacity:0, y:'-40'});
        timeline.from(
            [link1, link2, link3, link4], .3,
            {
                opacity:0,
                y:'-40',
                stagger: {
                    amount: .4
                }
            }
        );

    }, [timeline])*/
    return (
        <nav>
            <img src={Logo} alt="logo" width={146} height={74} ref={el => {logo = el}}/>
            <ul>
                <li className={"active"} ref={el => link1 = el}>ACCUEIL</li>
                <li ref={el => link2 = el}>SERVICES</li>
                <li ref={el => link3 = el}>A PROPOS</li>
                <li ref={el => link4 = el}>CONTACT</li>
            </ul>
        </nav>
    );
}

export default NavComponent;