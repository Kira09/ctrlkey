import React, {useRef, useEffect} from "react";
import {Routes, Route} from "react-router-dom";
import './App.scss'

import {HomePage, ContactPage} from "./pages/index.js";

function App() {
  return (
    <main>
        <Routes>
            <Route path={"/"} element={<HomePage/>}/>
            <Route path={"/contact"} element={<ContactPage/>}/>
        </Routes>
    </main>
  )
}

export default App
